%include "colon.inc"

extern read_word
extern exit
extern print_string
extern find_word
extern print_newline
extern string_length
global _start

section .data

	colon "first_time", first
	db "You're afraid of everything", 0

	colon "second_time", second
	db "You're afraid of everything, especially the don", 0

	colon "third_time", third
	db "Hope has always been weak. Crazy hope", 0

    reading_error_msg: db "Reading problem", 0
	not_found_msg: db "Key_not_found", 0
%define start_dict third
section .text
_start:
	mov rsi, 255
	sub rsp, 255
	mov rdi, rsp
	call read_word
	test rax,rax
	je .reading_error
	mov rdi,rax
	mov rsi, start_dict
	call find_word
	test rax,rax
	je .not_found
	add rax, 8
	mov rdi, rax
	mov r9,rax
	call string_length
	add r9, rax
	inc r9
	mov rdi, r9
	.exit:
		call print_string
		call print_newline
		call exit
	.reading_error:
		mov rdi,reading_error_msg
		jmp .exit
	.not_found:
		mov rdi,not_found_msg
		jmp .exit

extern string_equals
global find_word

section .text
; args: rdi = key string pointer, rsi = list first elem -> returns: rax = address (found) or rax = 0 (not found)
find_word:
	mov r8, rdi
	.loop:
		mov r9,rsi
		test rsi, rsi
		je .exit
		mov rdi, r8
		add rsi,8
		call string_equals
		mov rsi, r9
		test rax, rax
		jnz .exit
		mov rsi,[rsi]
		jmp .loop
	.exit:
		mov rax, rsi
		ret

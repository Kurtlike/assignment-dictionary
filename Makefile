asm = nasm
ASFLAGS = -felf64
objects = main.o dict.o lib.o

.PHONY: all clean

all: main 
	./main

main: $(objects) $(inc_objects)
	ld $^ -o $@ 

$(objects): %.o: %.asm 
	$(asm) $(ASFLAGS) $<

clean:
	find . -type f -name '*.o' -delete
